'use strict';

var kademlia = require('kad');
var inherits = require('util').inherits;
var async = require('async');

class FakeTransport {
  constructor(contact, options) {
    var self = this;

    // Make sure that it can be instantiated without the `new` keyword
    if (!(this instanceof FakeTransport)) {
      return new FakeTransport(contact, options);
    }

    // Call `kademlia.RPC` to setup bindings (super)
    kademlia.RPC.call(this, contact, options);
  }

  _open(ready) {
    FakeTransport.connections[this._contact.nodeID] = this;
    ready();
  }

  _send(data, contact) {
    async.nextTick(function sendFakeTransport() {
      FakeTransport.totalMessages++;
      FakeTransport.connections[contact.nodeID].receive(data);
    });
  }

  _close() {
    delete FakeTransport.connections[this._contact.nodeID];
  }
}

FakeTransport.connections = {};
FakeTransport.totalMessages = 0;

// Inherit for `kademlia.RPC`
inherits(FakeTransport, kademlia.RPC);

module.exports = FakeTransport;
