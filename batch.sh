#!/usr/bin/env sh

parallel --jobs -1 --joblog batch.log --nice 19 --linebuffer --progress --verbose "node simulator.js {1} {2} > data/{1}-{2}.json" ::: 1000 500 300 200 150 100 50 25 ::: 01 02 03

# https://www.gnu.org/software/parallel/parallel_tutorial.html
# parallel --retry-failed --joblog batch.log
