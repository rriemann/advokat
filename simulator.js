var async = require('async');
var kademlia = require('kad');
var faker = require('faker');
var spartacus = require('kad-spartacus');
var crypto = require('crypto');
var _ = require('lodash');
var ms = require('ms');
var assert = require('assert');
var FakeTransport = require('./fakeTransport');

var Peer = require('./peer');

var NUM_PEERS = 6;
if (!Number.isNaN(Number(process.argv[2]))) {
  NUM_PEERS = Number(process.argv[2]);
}

Peer.NOUNCE = '';
if (process.argv[3]) {
  Peer.NOUNCE = process.argv[3];
}

var simulation = {
  host: require('os').hostname(),
  revision: require('child_process').execSync('git rev-parse HEAD', {cwd: process.cwd()}).toString().trim(),
  num_peers: NUM_PEERS,
  nounce: Peer.NOUNCE,
  start_time: new Date()
}

var peers = [];

console.error("Create peers: %s", simulation.start_time);
while (peers.length < NUM_PEERS) {
  peers.push(new Peer(peers.length));
}

// Iterate over all of the created nodes and connect them in a chain to let
// them all begin to discover each other.

async.waterfall([
  // connect peers one by one
  function(nextWaterFall) {
    simulation.connect_peers_time = new Date();
    simulation.create_peers_duration = simulation.connect_peers_time - simulation.start_time;
    console.error("Peers join the network: %s", simulation.connect_peers_time);
    // kademlia.constants.T_RESPONSETIMEOUT = Math.floor(ms('1s')*NUM_PEERS*Math.log(NUM_PEERS)*0.04+7);
    kademlia.constants.T_RESPONSETIMEOUT = ms('20s');
    async.timesLimit(peers.length, NUM_PEERS, function(n, nextTimes) {
      var connectTo = (n == 0) ? 1 : n-1;
      peers[n].connect(peers[connectTo % peers.length].contact, nextTimes);
    }, nextWaterFall);
  },
  // refresh once again closest bucket
  /*
  function(peers, nextWaterFall) {
    console.log("Find own Node: %s", Date());
    async.mapLimit(peers, 1, function(peer, nextMap) {
      peer.node._router.findNode(peer.contact.nodeID, {
        aggressiveLookup: true
      }, function(err) {
        nextMap(err, peer);
      });
    }, nextWaterFall);
  },
  */
  // update buckets
  /*
  function(peers, nextWaterFall) {
    simulation.refresh_buckets_time = new Date();
    simulation.connect_peers_duration = simulation.refresh_buckets_time - simulation.connect_peers_time;
    console.error("Buckets refresh: %s", simulation.connect_peers_time);
    async.mapLimit(peers, 1, function(peer, nextMap) {
      var bucketIndexes = Object.keys(peer.node._router._buckets);
      var leastBucket = _.min(bucketIndexes);
      var refreshBuckets = _.range(Number(leastBucket), kademlia.constants.B);
      async.eachSeries(refreshBuckets, function(id, cb) {
        peer.node._router.refreshBucket(id, function(err, contacts) {
          _.forEach(contacts, function(contact) {
            var contact = peer.node._rpc._createContact(contact);
            peer.node._router.updateContact(contact);
          });
          cb(null);
        });
      }, function() {
        nextMap(null, peer);
      });

    }, nextWaterFall);
  },
  */
  // start aggregation procedure
  function(peers, nextWaterFall) {
    simulation.aggregate_time = new Date();
    simulation.refresh_buckets_duration = simulation.aggregate_time - simulation.refresh_buckets_time;
    simulation.connectMessageCount = FakeTransport.totalMessages;
    console.error("Peers aggregate: %s", simulation.aggregate_time);
    kademlia.constants.T_RESPONSETIMEOUT = ms('60s');
    spartacus.hooks.NONCE_EXPIRE = ms('24h');
    async.map(peers, function(peer, nextMap) {
      peer.aggregator.processAggregation(nextMap);
    }, nextWaterFall);
  },
  // disconnect all peers symoultanously
  function(results, nextWaterFall) {
    simulation.disconnect_time = new Date();
    simulation.aggregate_duration = simulation.disconnect_time - simulation.aggregate_time;
    console.error("Peers disconnect: %s", simulation.disconnect_time);
    async.each(peers, function(peer, nextEach) {
      peer.node.disconnect(nextEach);
    }, function(err) {
      nextWaterFall(err, results);
    });
  }
], function(err, results) {
  console.error("Disconnect done: %s", Date());
  // print some results
  if(err) {
    simulation.error = err.message;
    console.error('aggregation failed, reason %s', err.message);
  }

  simulation.totalMessageCount = FakeTransport.totalMessages;
  simulation.aggregationMessageCount = simulation.totalMessageCount - simulation.connectMessageCount;
  simulation.rootAggregateIDs = {}
  simulation.peers = [];
  _.forEach(peers, function(peer) {
    let info = peer.getInfo();
    simulation.rootAggregateIDs[peer.aggregator.resultContainer.iD] = (simulation.rootAggregateIDs[info.rootAggregateID] || 0) + 1;
    // console.log("result: %j (count: %d, leak: %d)", info.rootAggregateID, info.rootAggregateCounter, info.statistics.leakedInformation);
    simulation.peers.push(info);
  });
  console.log(JSON.stringify(simulation, null, 2));

  var counts = _.map(peers, function (peer) {return peer.aggregator.resultContainer.counter;});
  var meanCount = _.mean(counts);
  console.error("mean count: %d", meanCount);
  console.error("total Messages: %d", FakeTransport.totalMessages);
  _.forEach(peers, function(peer) {

  });
  if(Object.keys(simulation.rootAggregateIDs).length > 1) {
    console.error("ERR: more than one rootAggregate:\n%s", JSON.stringify(simulation.rootAggregateIDs, null, 2));
  }

  process.exit();
});
