def batchtest(counter)
  nounce = 0
  test = false
  begin
    nounce += 1
    puts "now with nounce #{nounce}"
    output = `node simulator.js #{counter} #{nounce}`.split("\n")[-1]
    test = (output =~ /#{counter}$/)
  end while test

  puts "nounce: #{nounce}"
end
