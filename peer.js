/**
* @module advokat/Peer
*/

'use strict';

var kademlia = require('kad');
var faker = require('faker');
var spartacus = require('kad-spartacus');
var crypto = require('crypto');
var akConstants = require('./constants');
var Aggregator = require('./aggregator');
var FakeTransport = require('./fakeTransport');

kademlia.constants.MESSAGE_TYPES.push(...akConstants.MESSAGE_TYPES);

var Contact = spartacus.ContactDecorator(kademlia.contacts.AddressPortContact);
class Peer {
  constructor(id) {
    this.id = id;
    // deterministic Ids
    var privKey = crypto.createHash('sha256').update(id.toString()+Peer.NOUNCE).digest();
    this.keypair = new spartacus.KeyPair(privKey);
    this.contact = Contact({
      address: '127.0.0.1',
      port: Peer.port-id,
      pubkey: this.keypair.getPublicKey()
    });
    // var transport = kademlia.transports.HTTP(this.contact);
    var transport = new FakeTransport(this.contact);
    // Sign your messages
    // transport.before('serialize', spartacus.hooks.sign(this.keypair));
    // Verify received messages
    // transport.before('receive', spartacus.hooks.verify(this.keypair));
    transport.before('receive', this.authoriseAggregateRequestHook());
    this.log = kademlia.Logger(2, 'NODE ' + id); // 4 means debug level, see logger.js
    this.node = kademlia.Node({
      storage: kademlia.storage.MemStore(),
      transport: transport,
      logger: this.log
    });

    this.aggregator = new Aggregator(this.node, this.contact);
    // this.log.info('created node with id: '+this.id);
  }

  authoriseAggregateRequestHook() {
    var peer = this;
    return function(message, contact, next) {
      if(message.method == 'QUERY') {
        peer.log.debug('authorisation check (dummy)');
      }
      next();
    };
  }

  connect(contact, callback) {
    // When a simulation node connects to another simulation node, store a random
    // phrase by a new UUID every STORE_INTERVAL, then fetch the value from peers
    // to make sure it was successfully stored.
    var peer = this;
    this.node.connect(contact, function onConnect(err) {
      var node = this;

      // if (err) {
      //   peer.log.error(err.message);
      //   process.exit();
      // }

      // var key = faker.random.uuid();
      // var value = faker.hacker.phrase();

      // setTimeout(function() {
      //   peer.aggregator.processAggregation();
      //
      //   // node.disconnect(function() {
      //   //   peer.log.info('left the network');
      //   // });
      // }, 1000);
      if(typeof callback === 'function') {
        callback(err, peer);
      }
    });
  }

  getInfo() {
    return {
      iD: this.id,
      statistics: this.aggregator.statistics.toObject(),
      rootAggregateID: this.aggregator.resultContainer.iD,
      rootAggregateCounter: this.aggregator.resultContainer.counter
    }
  }
}
Peer.port = 65535;

module.exports = Peer;
