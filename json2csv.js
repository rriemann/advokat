var json2csv = require('json2csv');
var _ = require('lodash');
var fs = require('fs');

var fields = [ 'host',
  'revision',
  'num_peers',
  'nounce',
  'start_time',
  'connect_peers_time',
  'create_peers_duration',
  'refresh_buckets_time',
  'connect_peers_duration',
  'aggregate_time',
  'refresh_buckets_duration',
  'connectMessageCount',
  'disconnect_time',
  'aggregate_duration',
  'totalMessageCount',
  'aggregationMessageCount',
  'rootAggregateIDs' ];

try {
  var fileNames = process.argv.slice(2);
  data = _.map(fileNames, function(fileName) {
    console.error("start extracting from %s", fileName);
    simulation = JSON.parse(fs.readFileSync(fileName, 'utf8'));
    delete simulation['peers'];
    return simulation;
  });
  var result = json2csv({ data: data, fields: fields, quotes: '' });
  console.log(result);
} catch (err) {
  // Errors are thrown for bad options, or if the data is empty and no fields are provided.
  // Be sure to provide fields if it is possible that your data array will be empty.
  console.error(err);
}
