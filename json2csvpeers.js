var fileName = process.argv[2];
var field = process.argv[3] || 'leakedInformation';
var _ = require('lodash');

var fs = require('fs');
var simulation = JSON.parse(fs.readFileSync(fileName, 'utf8'));

console.error("found %d peers", simulation.peers.length);
console.log("dist");
var array = []
_.forEach(simulation.peers, function(peer) {
  console.log(peer.statistics[field]);
  array.push(peer.statistics[field]);
});
console.error("min: %d max: %d mean: %d", _.min(array), _.max(array), _.mean(array));
