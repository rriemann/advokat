#!/usr/bin/env sh

for file in $@
do
  base=${file%.json}
  for field in receivedInformation leakedInformation inbox outbox
  do
    echo "start extracting from ${file}"
    node json2csvpeers.js $file $field > ${base}-${field}.csv
  done
done
